# PS5Kompat
Trying to make sure that script written for PowerShell Core (>=6.0) can start
through Windows PowerShell (<=5.0), or even if PowerShell Core is not yet
installed.

By default it checks for PowerShell Core 7.1.

## How to use?
The most simple way is to just include the file and run the function:
```PowerShell
. .\ps5kompat.ps1
Invoke-WithPwsh
```

If your file takes parameters, pass those to the function:
```PowerShell
. .\ps5kompat.ps1
Invoke-WithPwsh @PSBoundParameters
```

If you require a version of PowerShell other than 7.1, you can pass that when including the file:
```PowerShell
. .\ps5kompat.ps1 '7.3'
Invoke-WithPwsh
```

## How does it work?
@startuml
start
if (Is the running PowerShell recent enough?) is (<color:green>Yes) then
   :Return from Invoke-WithPwsh and continue running the script.;
else (<color:red>No)
   if(Is a recent-enough PowerShell Core installed?) is (<color:red>No) then
      :Install the current PowerShell Core.;
   else (<color:green>Yes)
   endif
   :Run the script through PowerShell Core.;
   :Exit the script entirely since it has just been run.;
endif
stop
@enduml