<#
    Include this file in your script and run Invoke-WithPwsh:
    . ./PS5Kompat.ps1
    Invoke-WithPwsh @PSBoundParameters
    
    Or:
    . ./PS5Kompat.ps1
    Invoke-WithPwsh
    if your script doesn't take parameters

    You can also supply a minimum version of PowerShell to run your script.
    Say you need version 7.5 or above, you can include the file with:
    . ./PS5Kompat.ps1 '7.5'

    Or:
    $reqdVer = [version]'7.5'
    . ./PS5Kompat.ps1 $reqdVer
#>
param (
    [Parameter()]
    [System.Management.Automation.SemanticVersion]
    $RequiredPwshVersion = '7.1'
)

function Invoke-WithPwsh {
    [console]::OutputEncoding = [console]::InputEncoding = $OutputEncoding = [System.Text.Encoding]::UTF8

    if ($PSVersionTable.PSVersion -lt $RequiredPwshVersion) {
        <#
        If the script is being run through an older version of PowerShell
        than required, try instead to run it through a more recent one.

        Try to install the current version if a recent-enough version is not found.
        #>

        Write-Output 'This seems to be an older version of PowerShell, trying to run the script through a more recent version.'
        
        # Need to use the Appx package to check whether a more recent version is installed,
        # but that is not available in PowerShell Core by default.
        If (($PSVersionTable.PSVersion -ge '6.0') -and -not (Get-Module Appx)) {
            Import-Module -Name Appx -UseWindowsPowerShell -WarningAction SilentlyContinue
        }

        # Check if we have a recent-enough version installed.
        $hasPwsh = Get-AppxPackage -name 'Microsoft.PowerShell'

        if ((-not $hasPwsh) -or ($hasPwsh.Version -lt $RequiredPwshVersion)) {
            Write-Host 'It seems like you do not have a recent version of PowerShell installed. Trying to install...'

            # Check to see if we have WinGet installed
            $hasPackageManager = Get-AppxPackage -name 'Microsoft.DesktopAppInstaller'
            if ((-not $hasPackageManager) -or ($hasPackageManager.Version -lt '1.10.0.0')) {
                # It seems we don't have WinGet, or it's too old
                Write-Output 'Installing WinGet in order to install PowerShell...'

                # Get the current CPU Architecture.
                # We get an integer from Get-WMIObject, but need a string.
                $CPUHash = @{0='x86';1='MIPS';2='Alpha';3='PowerPC';5='arm';6='ia64';9='x64';12='arm64'}
                $arch=$CPUHash[[int](Get-WMIObject -Class Win32_Processor -ErrorAction Stop).Architecture]

                # Check to see whether we have the correct UI XAML package for Winget, else install it.
                if (-not (Get-AppxPackage Microsoft.UI.Xaml.2.7)) {
                    Write-Output 'Installing WinGet prerequisite: Microsoft.UI.Xaml.2.7'
                    $xamlUrl = 'https://www.nuget.org/api/v2/package/Microsoft.UI.Xaml/2.7.2'
                    $xamlBase = "$($env:TEMP)\Microsoft.UI.Xaml.2.7.2"
                    $xamlZip = "$($xamlBase).zip"
                    $xamlAppx = "$($xamlBase)\tools\AppX\$($arch)\Release\Microsoft.UI.Xaml.2.7.appx"
                    Invoke-WebRequest $xamlUrl -OutFile $xamlZip -UseBasicParsing -ErrorAction Stop
                    Expand-Archive $xamlZip -DestinationPath $xamlBase -ErrorAction Stop
                    if (Test-Path $xamlAppx) {
                        Add-AppxPackage $xamlAppx -ErrorAction Stop
                    } else {
                        Throw "Something went wrong, cannot find $($xamlAppx)."
                    }
                }

                # Check to see whether we have the correct VisualC libs package for Winget, else install it.
                $vcLibs = Get-AppxPackage Microsoft.VCLibs.140.00.UWPDesktop
                if ((-not $vcLibs) -or ($vcLibs.Version -lt '14.0.30704.0')) {
                    Write-Host 'Installing WinGet prerequisite: Visual C++ 2015 UWP Desktop Runtime'
                    Add-AppxPackage -Path "https://aka.ms/Microsoft.VCLibs.$($arch).14.00.Desktop.appx" -ErrorAction Stop
                }

                # Install Winget
                $releases_url = 'https://api.github.com/repos/microsoft/winget-cli/releases/latest'
                [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
                $releases = Invoke-RestMethod -Uri $releases_url
                $latestRelease = $releases.assets | Where-Object { $_.browser_download_url.EndsWith('msixbundle') } | Select-Object -First 1
        
                Add-AppxPackage -Path $latestRelease.browser_download_url -ErrorAction Stop
            }

            # Install Microsoft PowerShell from Windows Store.
            winget install 9MZ1SNWT0N5D --accept-package-agreements --accept-source-agreements

            # Check to see if the installation worked.
            $hasPwsh = Get-AppxPackage -name 'Microsoft.PowerShell'
        }

        # If we have a recent enough PowerShell Core, and are NOT currently
        # using that, run the script through it.
        if ($hasPwsh -and ($hasPwsh.Version -ge $RequiredPwshVersion)) {
            # pwsh(.exe) is PowerShell Core (6 and above) (for Windows, macOS, Linux)
            # powershell(.exe) is Windows PowerShell (5.1 and below) (only Windows)

            pwsh (Get-Variable MyInvocation -Scope Local).Value.PSCommandPath @args
            Exit
        } else {
            $msg = 'Could not find a recent version of PowerShell on your machine, nor install one automatically.' + "`n" +
                'Please install the current version from Microsoft Store: https://www.microsoft.com/store/productId/9MZ1SNWT0N5D'
            Throw $msg
        }
    }
}